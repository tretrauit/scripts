# Scripts

Scripts I created for personal use.

## Directory structure

+ [apps](./apps/): Scripts for specific applications.
+ [games](./apps/): Scripts for specific games.
+ [userscripts](./userscripts/): UserScripts for specific web pages.
+ [userstyles](./userstyles/): UserStyles (or sometimes plain CSS) for specific web pages.
+ [web](./web/): Scripts meant to be executed in Console for specific web pages.

## License

[MIT](./LICENSE)
