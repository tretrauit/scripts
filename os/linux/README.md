# Linux-only scripts

+ [automount.py](https://git.tretrauit.me/tretrauit/scripts/raw/branch/main/os/linux/automount.py) ([source](./automount.py)): Automatically creates directory defined in `/etc/fstab` and mount them with `mount -a` 
