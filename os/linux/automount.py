#!/usr/bin/env python3
import sys
from pathlib import Path
from subprocess import Popen, PIPE, STDOUT

fstab_txt = Path("/etc/fstab").read_text()
for line in fstab_txt.splitlines():
    if line.startswith("#") or line.strip() == "":
        continue
    line_split = line.split(" ")
    if len(line_split) == 1:
        continue
    for text in line_split[1:]:
        if text.strip() == "":
            continue
        # text is probably the directory we wanted
        target_path = Path(text)
        if not target_path.is_dir():
            print(f"Touching directory: {target_path}")
            target_path.mkdir(parents=True, exist_ok=True)
        break

print("Mounting all directories...")
process = Popen("mount -a", shell=True, stdout=PIPE, stderr=STDOUT)
for line in iter(process.stdout.readline, b""):
    sys.stdout.write(line.decode())
