# UserStyles

Contains random UserStyles.

## UserStyles list

- [Messenger Dynamic Sidebar](./messenger-dynamic-sidebar.user.css): [reizumi](https://codeberg.org/reizumi)'s Messenger Dynamic Sidebar ported to pure CSS for Ferdium support
  > Ported from commit [`9df0607a9c709ba5a1e8e3d12e98c36e64928981`](https://codeberg.org/reizumi/userstyles/commit/9df0607a9c709ba5a1e8e3d12e98c36e64928981) using [update-messenger-dynamic-sidebar.py](./scripts/update-messenger-dynamic-sidebar.py)
  
  > If the CSS isn't working as expected, try pasting it into `darkmode.css` and Enable Dark mode in Ferdium.
