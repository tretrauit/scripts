import requests
from pathlib import Path


URL = "https://codeberg.org/reizumi/userstyles/raw/branch/master/messenger-dynamic-sidebar.user.css"


def find_line(source: str, content: str) -> int:
    for line, text in enumerate(source.splitlines()):
        if content in text:
            return line
    return -1


def replace_bulk(css: str, replacements: dict) -> str:
    for k, v in replacements.items():
        css = css.replace(k, v)
    return css


def main():
    print("Reading our CSS file...")
    my_css = (Path.cwd() / "messenger-dynamic-sidebar.user.css").read_text()
    my_css_lines = my_css.splitlines()
    print("Reading the source CSS file...")
    r = requests.get(URL)
    r.raise_for_status()
    source_css = r.text
    source_css_lines = source_css.splitlines()
    my_new_css = ""
    # Update CSS addresses
    css_class_names = {}
    src_begin_name_map = False
    for line, text in enumerate(source_css_lines):
        if text.strip() == "/* MDS classes */":
            src_begin_name_map = True
        elif src_begin_name_map:
            if text.strip() == "":
                src_begin_name_map = False
                break
            print("Fixing MDS classes...")
            splitted = text.split("=", 1)
            var_name = "{" + splitted[0].strip() + "}"
            # [1:-2] to remove the quotes and the ";"
            var_value = splitted[1].strip()[1:-2]
            css_class_names[var_name] = var_value
    skip_lines = []
    for line, text in enumerate(my_css_lines):
        if line in skip_lines:
            print("Skipping line", line)
            continue
        elif text.strip() == "/* Hide action bar */":
            print("Fixing hide action bar CSS...")
            hide_action_line = find_line(source_css, "if hideActionBar {")
            if hide_action_line != -1:
                my_new_css += text + "\n" + source_css_lines[hide_action_line + 1].strip() + "\n"
                hover_line = hide_action_line + 8
                skip_lines.append(line + 1)
                for i in range(2, 8):
                    my_new_css += my_css_lines[line + i] + "\n"
                    skip_lines.append(line + i)
                my_new_css += source_css_lines[hover_line].strip() + "\n"
                skip_lines.append(line + 8)
        elif text.strip().startswith("/*"):
            source_line = find_line(source_css, text)
            if source_line != -1:
                print(f"Found line for {text}")
                replaced_line = replace_bulk(source_css_lines[source_line + 1].strip(), css_class_names)
                print(replaced_line)
                my_new_css += text + "\n" + replaced_line + "\n"
                next_line = line + 1
                skip_lines.append(next_line)
                while not my_css_lines[next_line].endswith("{"):
                    next_line += 1
                    skip_lines.append(next_line)
            else:
                print(f"Couldn't find line for {text}")
                my_new_css += text + "\n"
        else:
            my_new_css += text + "\n"
    print()
    print("=== OUTPUT CSS ===")
    print()
    print(my_new_css)
    print("=== END OUTPUT CSS ===")
    print()
    print("Writing new CSS file...")
    (Path.cwd() / "messenger-dynamic-sidebar.user.css").write_text(my_new_css)


if __name__ == "__main__":
    main()
