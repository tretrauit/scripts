// Dependencies
eval(
  await (
    await fetch("https://html2canvas.hertzen.com/dist/html2canvas.min.js")
  ).text(),
);

async function screenshotAnswer(answerElement) {
  console.log(answerElement);
  const canvas = await html2canvas(answerElement);
  return await canvas.toDataURL();
}

const answerCol = document.querySelector("#list-question");
// Patch the style to make it show all answers
answerCol.style.maxHeight = "10000vh";
const answerImgData = await screenshotAnswer(answerCol);
// Create an image
const img = new Image();
img.src = answerImgData;
const w = window.open("");
// Write the image to the new window
w.document.write(img.outerHTML);
w.document.close();
