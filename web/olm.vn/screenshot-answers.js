// Dependencies
eval(
  await (
    await fetch("https://html2canvas.hertzen.com/dist/html2canvas.min.js")
  ).text(),
);
eval(
  await (
    await fetch("https://cdn.jsdelivr.net/npm/jszip@3.10.1/dist/jszip.min.js")
  ).text(),
);
eval(
  await (
    await fetch(
      "https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.0/FileSaver.min.js",
    )
  ).text(),
);

const zipFile = new JSZip();

async function screenshotAnswer(answerElement) {
  console.log(answerElement);
  const canvas = await html2canvas(answerElement);
  return await new Promise((resolve) => canvas.toBlob(resolve));
}

async function saveImage(image, imageName) {
  zipFile.file(imageName, image);
}

async function saveZip() {
  const generatedFile = await zipFile.generateAsync({ type: "blob" });
  console.log(`URL: ${URL.createObjectURL(generatedFile)}`);
  saveAs(generatedFile, "olm-answers.zip");
}

const questionBtns = document.querySelector("#question-static").children;
const wrongQuestionCount =
  document.querySelectorAll(".q-static.q-wrong").length;
for (const [index, questionBtn] of Array.from(questionBtns).entries()) {
  if (index > questionBtns.length - 1 - wrongQuestionCount) {
    break;
  }
  // Click the button
  questionBtn.click();
  const answerElement =
    document.querySelector("#qholder").parentElement.parentElement;
  const answerImage = await screenshotAnswer(answerElement);
  saveImage(answerImage, `question-${index + 1}.png`);
}
await saveZip();
