// ==UserScript==
// @name        AI notifier for Pixiv
// @description Notify you when an AI artwork is detected (through tags) on Pixiv
// @namespace   tretrauit-dev
// @match       *://www.pixiv.net/*
// @icon        https://upload.wikimedia.org/wikipedia/commons/7/7e/Pixiv_Icon.svg
// @grant       none
// @version     1.0.3
// @author      tretrauit
// @run-at      document-idle
// @homepageURL https://gitlab.com/tretrauit/scripts
// @supportURL  https://gitlab.com/tretrauit/scripts/-/issues
// @downloadURL https://git.tretrauit.me/tretrauit/scripts/raw/branch/main/userscripts/pixiv-ai-notifier.user.js
// ==/UserScript==

function checkAI() {
	if (!window.location.pathname.includes("/artworks/")) {
		return;
	}
	const tagElms = document.querySelectorAll(".gtm-new-work-tag-event-click");
	for (const elm of tagElms) {
		const parentElm = elm.parentElement.parentElement;
		for (const childElm of parentElm.children) {
			const text = childElm.innerText.trim();
			const textLowerCase = text.toLowerCase();
			if (
				text.startsWith("AI") ||
				text.endsWith("AI") ||
				textLowerCase === "ai" ||
				(textLowerCase.includes("ai") &&
					(textLowerCase.includes("generated") ||
						textLowerCase.includes("illustration")))
			) {
				alert("AI artwork detected :(");
				return;
			}
		}
	}
}

// Stack Overflow thingy
let previousUrl = "";

const observer = new MutationObserver(() => {
	if (window.location.href !== previousUrl) {
		console.log(`URL changed from ${previousUrl} to ${window.location.href}`);
		previousUrl = window.location.href;
		// do your thing
		setTimeout(checkAI, 1000);
	}
});
const config = { subtree: true, childList: true };

// start observing change
console.log("AI notifier for Pixiv is running...");
observer.observe(document, config);
setTimeout(checkAI, 1000);
