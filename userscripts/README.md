# UserScripts

Useful UserScripts that I use everyday.

## Scripts

+ [anonyviet-skip-wait.user.js](https://git.tretrauit.me/tretrauit/scripts/raw/branch/main/userscripts/anonyviet-skip-wait.user.js) ([source](./anonyviet-skip-wait.user.js)): Skip the annoying 15s wait in AnonyViet
+ [genshin-concert2022-wallpaper-mode.user.js](https://git.tretrauit.me/tretrauit/scripts/raw/branch/main/userscripts/genshin-concert2022-wallpaper-mode.user.js) ([source](./genshin-concert2022-wallpaper-mode.user.js)): Wallpaper mode for https://genshin.hoyoverse.com/concert2022 (Genshin 2022 concert)
+ [hidemy.name-free-ipport-export.user.js](https://git.tretrauit.me/tretrauit/scripts/raw/branch/main/userscripts/hidemy.name-free-ipport-export.user.js) ([source](./hidemy.name-free-ipport-export.user.js)):  Free export IP:Port button in hidemy.name
+ [kgvn-8thang5-autofarm.user.js](https://git.tretrauit.me/tretrauit/scripts/raw/branch/main/userscripts/kgvn-8thang5-autofarm.user.js) ([source](./kgvn-8thang5-autofarm.user.js)): Auto farm for a certain web event in AoV Vietnamese version.
+ [pixiv-ai-notifier.user.js](https://git.tretrauit.me/tretrauit/scripts/raw/branch/main/userscripts/pixiv-ai-notifier.user.js) ([source](./pixiv-ai-notifier.user.js)): Notifies you when the artwork you're viewing is an AI-generated one.
+ [tiktok.user.js](https://git.tretrauit.me/tretrauit/scripts/raw/branch/main/userscripts/tiktok.user.js) ([source](./tiktok.user.js)): Enhance TikTok experience on PC, currently only removes the "Download app" banner.
+ [yt-noshorts.user.js](https://git.tretrauit.me/tretrauit/scripts/raw/branch/main/userscripts/yt-noshorts.user.js) ([source](./yt-noshorts.user.js)): Redirects Shorts to normal video webpage in YouTube.
